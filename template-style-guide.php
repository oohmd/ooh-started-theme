<?php
/**
 * Template Name: Style Guide
 */
?>
<style type="text/css">
	.subheader {position: relative;} .subheader>span{background: #fff;padding-right: 1rem;} .subheader:after {position: absolute; width: 100%; height: 1px; background: #111; content: ""; top: 50%; left: 0; z-index: -1; } 
</style>

<h1>Guia de estilos</h1>

<div class="container">
	<div class="row">
		<div class="col-12 mt-2">
			<h2>Tipografía</h2>
			<p class="subheader"><span>Headings</span></p>
		</div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 hidden-xs"><span>Header 1</span></div>
		<div class="col-xs-10 col-lg-10 col-md-10 col-sm-10 hidden-xs"><h1>h1. Lorem ipsum dolor sit amet</h1></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 hidden-xs"><span>Header 2</span></div>
		<div class="col-xs-10 col-lg-10 col-md-10 col-sm-10 hidden-xs"><h2>h2. Lorem ipsum dolor sit amet</h2></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 hidden-xs"><span>Header 3</span></div>
		<div class="col-xs-10 col-lg-10 col-md-10 col-sm-10 hidden-xs"><h3>h3. Lorem ipsum dolor sit amet</h3></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 hidden-xs"><span>Header 4</span></div>
		<div class="col-xs-10 col-lg-10 col-md-10 col-sm-10 hidden-xs"><h4>h4. Lorem ipsum dolor sit amet</h4></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 hidden-xs"><span>Header 5</span></div>
		<div class="col-xs-10 col-lg-10 col-md-10 col-sm-10 hidden-xs"><h5>h5. Lorem ipsum dolor sit amet</h5></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 hidden-xs"><span>Header 6</span></div>
		<div class="col-xs-10 col-lg-10 col-md-10 col-sm-10 hidden-xs"><h6>h6. Lorem ipsum dolor sit amet</h6></div>
	</div>
	<div class="row">
		<div class="col-12 mt-2">
			<p class="subheader"><span>Displays (.display-{1,2,3,4})</span></p>
		</div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 hidden-xs"><span>Display 1</span></div>
		<div class="col-xs-10 col-lg-10 col-md-10 col-sm-10 hidden-xs"><p class="display-1">Lorem ipsum dolor</p></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 hidden-xs"><span>Display 2</span></div>
		<div class="col-xs-10 col-lg-10 col-md-10 col-sm-10 hidden-xs"><p class="display-2">Lorem ipsum dolor</p></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 hidden-xs"><span>Display 3</span></div>
		<div class="col-xs-10 col-lg-10 col-md-10 col-sm-10 hidden-xs"><p class="display-3">Lorem ipsum dolor</p></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 hidden-xs"><span>Display 4</span></div>
		<div class="col-xs-10 col-lg-10 col-md-10 col-sm-10 hidden-xs"><p class="display-4">Lorem ipsum dolor</p></div>
	</div>
	<div class="row">
		<div class="col-12">
			<p class="subheader"><span>Parrafos (&lt;p&gt;&lt;/p&gt;)</span></p>
		</div>
		<div class="col-12">
			<p>Ut ac ante et risus molestie viverra non sed enim. Cras nisi nisi, varius ut euismod id, tincidunt eget diam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse enim elit, maximus eget risus a, commodo pretium nisl. Morbi eget blandit turpis, non pellentesque tortor. Pellentesque efficitur eros est, sed dapibus metus egestas nec. Maecenas sit amet commodo nibh. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id lacus ligula.</p>
			<p>Integer vel aliquam magna. Proin tincidunt finibus nunc eu iaculis. Pellentesque quis ipsum dui. Nunc id bibendum leo. Donec at nulla vitae nulla mattis elementum a ac ipsum. Vestibulum maximus consequat dui at porttitor. Praesent id odio tortor. Duis eget posuere metus, vitae iaculis dui. Nulla lacinia ac leo ac aliquam. Curabitur aliquet aliquet dapibus. Morbi vitae tincidunt ligula. Sed vehicula viverra purus in tristique.</p>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<p class="subheader"><span>Blockquotes</span></p>
		</div>
		<div class="col-12">
			<blockquote class="blockquote">
				<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
			</blockquote> <br />
			<blockquote class="blockquote">
				<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
				<footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
			</blockquote>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<p class="subheader"><span>Listas</span></p>
		</div>
		<div class="col-6">
			<b>Natural ul</b>
			<ul>
				<li>Lorem ipsum dolor sit amet</li>
				<li>Consectetur adipiscing elit</li>
				<li>Integer molestie lorem at massa</li>
				<li>Facilisis in pretium nisl aliquet</li>
				<li>Nulla volutpat aliquam velit
					<ul>
						<li>Phasellus iaculis neque</li>
						<li>Purus sodales ultricies</li>
						<li>Vestibulum laoreet porttitor sem</li>
						<li>Ac tristique libero volutpat at</li>
					</ul>
				</li>
				<li>Faucibus porta lacus fringilla vel</li>
				<li>Aenean sit amet erat nunc</li>
				<li>Eget porttitor lorem</li>
			</ul>
		</div>
		<div class="col-6">
			<b>Natural ol</b>
			<ol>
				<li>Lorem ipsum dolor sit amet</li>
				<li>Consectetur adipiscing elit</li>
				<li>Integer molestie lorem at massa</li>
				<li>Facilisis in pretium nisl aliquet</li>
				<li>Nulla volutpat aliquam velit
					<ul>
						<li>Phasellus iaculis neque</li>
						<li>Purus sodales ultricies</li>
						<li>Vestibulum laoreet porttitor sem</li>
						<li>Ac tristique libero volutpat at</li>
					</ul>
				</li>
				<li>Faucibus porta lacus fringilla vel</li>
				<li>Aenean sit amet erat nunc</li>
				<li>Eget porttitor lorem</li>
			</ol>
		</div>
		<div class="col-12">
			<b>.list-unstyled</b>
			<ul class="list-unstyled">
				<li>Lorem ipsum dolor sit amet</li>
				<li>Consectetur adipiscing elit</li>
				<li>Integer molestie lorem at massa</li>
				<li>Facilisis in pretium nisl aliquet</li>
				<li>Nulla volutpat aliquam velit
					<ul>
						<li>Phasellus iaculis neque</li>
						<li>Purus sodales ultricies</li>
						<li>Vestibulum laoreet porttitor sem</li>
						<li>Ac tristique libero volutpat at</li>
					</ul>
				</li>
				<li>Faucibus porta lacus fringilla vel</li>
				<li>Aenean sit amet erat nunc</li>
				<li>Eget porttitor lorem</li>
			</ul>
		</div>
		<div class="col-12">
			<b>.list-inline</b>
			<ul class="list-inline">
				<li class="list-inline-item">Lorem ipsum</li>
				<li class="list-inline-item">Phasellus iaculis</li>
				<li class="list-inline-item">Nulla volutpat</li>
			</ul>
		</div>
		<div class="col-12 mt-2">
			<p class="subheader"><span>List group</span></p>
		</div>
		<div class="col-6 mb-3">
			<b>Basico</b>
			<ul class="list-group mt-2">
				<li class="list-group-item">Cras justo odio</li>
				<li class="list-group-item">Dapibus ac facilisis in</li>
				<li class="list-group-item">Morbi leo risus</li>
				<li class="list-group-item">Porta ac consectetur ac</li>
				<li class="list-group-item">Vestibulum at eros</li>
			</ul>
		</div>
		<div class="col-6 mb-3">
			<b>Active items</b>
			<ul class="list-group mt-2">
				<li class="list-group-item active">Cras justo odio</li>
				<li class="list-group-item">Dapibus ac facilisis in</li>
				<li class="list-group-item">Morbi leo risus</li>
				<li class="list-group-item">Porta ac consectetur ac</li>
				<li class="list-group-item">Vestibulum at eros</li>
			</ul>
		</div>
		<div class="col-6 mb-3">
			<b>Contenido personalizado</b>
			<div class="list-group mt-2">
				<a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
					<div class="d-flex w-100 justify-content-between">
						<h5 class="mb-1">List group item heading</h5>
						<small>3 days ago</small>
					</div>
					<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
					<small>Donec id elit non mi porta.</small>
				</a>
				<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
					<div class="d-flex w-100 justify-content-between">
						<h5 class="mb-1">List group item heading</h5>
						<small class="text-muted">3 days ago</small>
					</div>
					<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
					<small class="text-muted">Donec id elit non mi porta.</small>
				</a>
				<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
					<div class="d-flex w-100 justify-content-between">
						<h5 class="mb-1">List group item heading</h5>
						<small class="text-muted">3 days ago</small>
					</div>
					<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
					<small class="text-muted">Donec id elit non mi porta.</small>
				</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12 mt-2">
			<h2>Enlaces</h2>
			<p class="subheader"><span>Link</span></p>
		</div>
		<div class="col-12 mb-3">
			<p><a href="#">Default link (without class)</a></p>
			<p><a href="#" class="text-primary">Primary link</a></p>
			<p><a href="#" class="text-secondary">Secondary link</a></p>
			<p><a href="#" class="text-success">Success link</a></p>
			<p><a href="#" class="text-danger">Danger link</a></p>
			<p><a href="#" class="text-warning">Warning link</a></p>
			<p><a href="#" class="text-info">Info link</a></p>
			<p><a href="#" class="text-light bg-gray">Light link</a></p>
			<p><a href="#" class="text-dark">Dark link</a></p>
		</div>
		<div class="col-12 mt-2">
			<p class="subheader"><span>Breadcrumb</span></p>
			<div class="col-12 mb-3">
				<ol class="breadcrumb">
					<li class="breadcrumb-item active">Home</li>
				</ol>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">Library</li>
				</ol>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Library</a></li>
					<li class="breadcrumb-item active">Data</li>
				</ol>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<h2>Botones</h2>
			<p class="subheader"><span>Default (.btn)</span></p>
		</div>
		<div class="col-12 mb-3">
			<button type="button" class="btn btn-primary">Primary</button>
			<button type="button" class="btn btn-secondary">Secondary</button>
			<button type="button" class="btn btn-success">Success</button>
			<button type="button" class="btn btn-danger">Danger</button>
			<button type="button" class="btn btn-warning">Warning</button>
			<button type="button" class="btn btn-info">Info</button>
			<button type="button" class="btn btn-light">Light</button>
			<button type="button" class="btn btn-dark">Dark</button>
			<button type="button" class="btn btn-link">Link</button><br />
		</div>
		<div class="col-12">
			<p class="subheader"><span>Round (.btn .btn-round)</span></p>
		</div>
		<div class="col-12 mb-3">
			<button type="button" class="btn btn-round btn-primary">Primary</button>
			<button type="button" class="btn btn-round btn-secondary">Secondary</button>
			<button type="button" class="btn btn-round btn-success">Success</button>
			<button type="button" class="btn btn-round btn-danger">Danger</button>
			<button type="button" class="btn btn-round btn-warning">Warning</button>
			<button type="button" class="btn btn-round btn-info">Info</button>
			<button type="button" class="btn btn-round btn-light">Light</button>
			<button type="button" class="btn btn-round btn-dark">Dark</button>
			<button type="button" class="btn btn-round btn-link">Link</button><br />
		</div>
		<div class="col-12">
			<p class="subheader"><span>Outline (.btn [.btn-round] .btn-outline-{type})</span></p>
		</div>
		<div class="col-12 mb-3">
			<button type="button" class="btn btn-outline-primary">Primary</button>
			<button type="button" class="btn btn-outline-secondary">Secondary</button>
			<button type="button" class="btn btn-outline-success">Success</button>
			<button type="button" class="btn btn-outline-danger">Danger</button>
			<button type="button" class="btn btn-outline-warning">Warning</button>
			<button type="button" class="btn btn-outline-info">Info</button>
			<button type="button" class="btn btn-outline-light">Light</button>
			<button type="button" class="btn btn-outline-dark">Dark</button>
			<button type="button" class="btn btn-outline-link">Link</button><br />
		</div>
		<div class="col-12 mb-3">
			<button type="button" class="btn btn-outline-round btn-primary">Primary</button>
			<button type="button" class="btn btn-outline-round btn-secondary">Secondary</button>
			<button type="button" class="btn btn-outline-round btn-success">Success</button>
			<button type="button" class="btn btn-outline-round btn-danger">Danger</button>
			<button type="button" class="btn btn-outline-round btn-warning">Warning</button>
			<button type="button" class="btn btn-outline-round btn-info">Info</button>
			<button type="button" class="btn btn-outline-round btn-light">Light</button>
			<button type="button" class="btn btn-outline-round btn-dark">Dark</button>
			<button type="button" class="btn btn-outline-round btn-link">Link</button><br />
		</div>
		<div class="col-12">
			<p class="subheader"><span>Sizes</span></p>
		</div>
		<div class="col-12 mb-3">
			<button type="button" class="btn btn-primary btn-lg">Large button</button>
			<button type="button" class="btn btn-secondary btn-lg">Large button</button><br />
		</div>
		<div class="col-12 mb-3">
			<button type="button" class="btn btn-primary btn-sm">Small button</button>
			<button type="button" class="btn btn-secondary btn-sm">Small button</button><br />
		</div>
		<div class="col-12 mb-3">
			<button type="button" class="btn btn-primary btn-lg btn-block">Block level button</button>
			<button type="button" class="btn btn-secondary btn-lg btn-block">Block level button</button><br />
		</div>
		<div class="col-12">
			<p class="subheader"><span>Grupos</span></p>
		</div>
		<div class="col-4 mb-3">
			<div class="btn-group" role="group" aria-label="Basic example">
				<button type="button" class="btn btn-secondary">Left</button>
				<button type="button" class="btn btn-secondary">Middle</button>
				<button type="button" class="btn btn-secondary">Right</button>
			</div>
		</div>
		<div class="col-4 mb-3">
			<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
				<div class="btn-group mr-2" role="group" aria-label="First group">
					<button type="button" class="btn btn-secondary">1</button>
					<button type="button" class="btn btn-secondary">2</button>
					<button type="button" class="btn btn-secondary">3</button>
					<button type="button" class="btn btn-secondary">4</button>
				</div>
				<div class="btn-group mr-2" role="group" aria-label="Second group">
					<button type="button" class="btn btn-secondary">5</button>
					<button type="button" class="btn btn-secondary">6</button>
					<button type="button" class="btn btn-secondary">7</button>
				</div>
				<div class="btn-group" role="group" aria-label="Third group">
					<button type="button" class="btn btn-secondary">8</button>
				</div>
			</div>
		</div>
		<div class="col-4 mb-3">
			<div class="btn-group" role="group" aria-label="Button group with nested dropdown">
				<button type="button" class="btn btn-secondary">1</button>
				<button type="button" class="btn btn-secondary">2</button>

				<div class="btn-group" role="group">
					<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Dropdown
					</button>
					<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
						<a class="dropdown-item" href="#">Dropdown link</a>
						<a class="dropdown-item" href="#">Dropdown link</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12">
			<p class="subheader"><span>Paginación</span></p>
		</div>
		<div class="col-12 mb-3">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					<li class="page-item disabled">
						<span class="page-link">Previous</span>
					</li>
					<li class="page-item"><a class="page-link" href="#">1</a></li>
					<li class="page-item active">
						<span class="page-link">
							2
							<span class="sr-only">(current)</span>
						</span>
					</li>
					<li class="page-item"><a class="page-link" href="#">3</a></li>
					<li class="page-item">
						<a class="page-link" href="#">Next</a>
					</li>
				</ul>
			</nav>
		</div>
		<div class="col-12 mb-3">
			<nav aria-label="Page navigation example">
				<ul class="pagination justify-content-center">
					<li class="page-item disabled">
						<span class="page-link">Previous</span>
					</li>
					<li class="page-item"><a class="page-link" href="#">1</a></li>
					<li class="page-item active">
						<span class="page-link">
							2
							<span class="sr-only">(current)</span>
						</span>
					</li>
					<li class="page-item"><a class="page-link" href="#">3</a></li>
					<li class="page-item">
						<a class="page-link" href="#">Next</a>
					</li>
				</ul>
			</nav>
		</div>
		<div class="col-12 mb-3">
			<nav aria-label="Page navigation example">
				<ul class="pagination justify-content-end">
					<li class="page-item disabled">
						<span class="page-link">Previous</span>
					</li>
					<li class="page-item"><a class="page-link" href="#">1</a></li>
					<li class="page-item active">
						<span class="page-link">
							2
							<span class="sr-only">(current)</span>
						</span>
					</li>
					<li class="page-item"><a class="page-link" href="#">3</a></li>
					<li class="page-item">
						<a class="page-link" href="#">Next</a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

	<div class="row">
		<div class="col-12 mt-2">
			<h2>Formularios</h2>
		</div>
		<div class="col-12 mb-3">
			<form>
				<div class="form-group">
					<label class="form-control-label" for="formGroupExampleInput">Example label</label>
					<input type="text" class="form-control" id="formGroupExampleInput" placeholder="Example input">
				</div>
				<div class="form-group">
					<label for="exampleFormControlInput1">Email address</label>
					<input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
				</div>
				<div class="form-group">
					<label for="inputPassword4" class="col-form-label">Example Password input</label>
					<input type="password" class="form-control" id="inputPassword4" placeholder="Password">
				</div>
				<div class="form-group">
					<label for="inlineFormInputGroup" class="col-form-label">Example Input Group</label>
					<div class="input-group mb-2 mb-sm-0">
						<div class="input-group-addon">@</div>
						<input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Username">
					</div>
				</div>
				<div class="form-group">
					<label for="exampleFormControlSelect1">Example select</label>
					<select class="form-control" id="exampleFormControlSelect1">
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
				</div>
				<div class="form-group">
					<label for="exampleFormControlSelect2">Example multiple select</label>
					<select multiple class="form-control" id="exampleFormControlSelect2">
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
				</div>
				<div class="form-group">
					<label for="exampleFormControlTextarea1">Example textarea</label>
					<textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
				</div>
				<div class="form-group">
					<div class="form-check">
						<label class="form-check-label">
							<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
							Option one is this and that&mdash;be sure to include why it's great
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
							Option two can be something else and selecting it will deselect option one
						</label>
					</div>
					<div class="form-check disabled">
						<label class="form-check-label">
							<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" disabled>
							Option three is disabled
						</label>
					</div>
				</div>
				<div class="form-group">
					<div class="form-check form-check-inline">
						<label class="form-check-label">
							<input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"> 1
						</label>
					</div>
					<div class="form-check form-check-inline">
						<label class="form-check-label">
							<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2"> 2
						</label>
					</div>
					<div class="form-check form-check-inline disabled">
						<label class="form-check-label">
							<input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3" disabled> 3
						</label>
					</div>
				</div>
				<div class="form-group">
					<div class="form-check form-check-inline">
						<label class="form-check-label">
							<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> 1
						</label>
					</div>
					<div class="form-check form-check-inline">
						<label class="form-check-label">
							<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> 2
						</label>
					</div>
					<div class="form-check form-check-inline disabled">
						<label class="form-check-label">
							<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3" disabled> 3
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="exampleFormControlFile1">Example file input</label>
					<input type="file" class="form-control-file" id="exampleFormControlFile1">
				</div>
				<div class="form-group">
					<label for="inputPassword5">Example input helper</label>
					<input type="password" id="inputPassword5" class="form-control" aria-describedby="passwordHelpBlock">
					<small id="passwordHelpBlock" class="form-text text-muted">
						Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji.
					</small>
				</div>
			</form>
			<form class="form-inline">
				<div class="form-group">
					<label for="inputPassword6">Password</label>
					<input type="password" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline">
					<small id="passwordHelpInline" class="text-muted">
						Must be 8-20 characters long.
					</small>
				</div>
			</form>
		</div>
	</div>

	<div class="row">
		<div class="col-12 mt-2">
			<h2>Modal</h2>
			<p class="subheader"><span>Ejemplo</span></p>
		</div>
		<div class="col-12 mb-3">
			<div class="modal" style="display: block;position: relative;">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Modal title</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<p>Modal body text goes here.</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary">Save changes</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12">
			<p class="subheader"><span>Paginación</span></p>
		</div>
		<div class="col-12 mb-3">
			<!-- Button trigger modal -->
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
				Launch demo modal
			</button>

			<!-- Modal -->
			<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							...
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Save changes</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 mb-3">
			<!-- Large modal -->
			<button class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Large modal</button>

			<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							...
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Save changes</button>
						</div>
					</div>
				</div>
			</div>

			<!-- Small modal -->
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-sm">Small modal</button>

			<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							...
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Save changes</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-12 mt-2">
			<h2>Menús</h2>
			<p class="subheader"><span>Tabs</span></p>
		</div>
		<div class="col-12 mb-3">
			<ul class="nav nav-tabs d-flex" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-expanded="true">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile">Profile</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
						Dropdown
					</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" id="dropdown1-tab" href="#dropdown1" role="tab" data-toggle="tab" aria-controls="dropdown1">@fat</a>
						<a class="dropdown-item" id="dropdown2-tab" href="#dropdown2" role="tab" data-toggle="tab" aria-controls="dropdown2">@mdo</a>
					</div>
				</li>
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">Consequat occaecat ullamco amet non eiusmod nostrud dolore irure incididunt est duis anim sunt officia. Fugiat velit proident aliquip nisi incididunt nostrud exercitation proident est nisi. Irure magna elit commodo anim ex veniam culpa eiusmod id nostrud sit cupidatat in veniam ad. Eiusmod consequat eu adipisicing minim anim aliquip cupidatat culpa excepteur quis. Occaecat sit eu exercitation irure Lorem incididunt nostrud.</div>
				<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">Ad pariatur nostrud pariatur exercitation ipsum ipsum culpa mollit commodo mollit ex. Aute sunt incididunt amet commodo est sint nisi deserunt pariatur do. Aliquip ex eiusmod voluptate exercitation cillum id incididunt elit sunt. Qui minim sit magna Lorem id et dolore velit Lorem amet exercitation duis deserunt. Anim id labore elit adipisicing ut in id occaecat pariatur ut ullamco ea tempor duis.</div>
				<div class="tab-pane fade" id="dropdown1" role="tabpanel" aria-labelledby="dropdown1-tab">Est quis nulla laborum officia ad nisi ex nostrud culpa Lorem excepteur aliquip dolor aliqua irure ex. Nulla ut duis ipsum nisi elit fugiat commodo sunt reprehenderit laborum veniam eu veniam. Eiusmod minim exercitation fugiat irure ex labore incididunt do fugiat commodo aliquip sit id deserunt reprehenderit aliquip nostrud. Amet ex cupidatat excepteur aute veniam incididunt mollit cupidatat esse irure officia elit do ipsum ullamco Lorem. Ullamco ut ad minim do mollit labore ipsum laboris ipsum commodo sunt tempor enim incididunt. Commodo quis sunt dolore aliquip aute tempor irure magna enim minim reprehenderit. Ullamco consectetur culpa veniam sint cillum aliqua incididunt velit ullamco sunt ullamco quis quis commodo voluptate. Mollit nulla nostrud adipisicing aliqua cupidatat aliqua pariatur mollit voluptate voluptate consequat non.</div>
				<div class="tab-pane fade" id="dropdown2" role="tabpanel" aria-labelledby="dropdown2-tab">Tempor anim aliquip qui nisi sit minim ex in cupidatat ipsum adipisicing. Ad non magna anim id ullamco do dolor sit adipisicing nulla exercitation. Qui Lorem eiusmod sint in laboris pariatur est adipisicing non sunt occaecat in mollit culpa sit. Aliquip id duis do do quis mollit ut duis. Non dolor reprehenderit do esse nostrud deserunt non eiusmod minim anim sit voluptate ipsum. Nulla elit aliqua do sunt labore velit anim nisi dolor nostrud consectetur fugiat ex qui velit ex tempor. Do cillum qui anim aliquip id cillum duis ex laboris tempor incididunt sint dolor ullamco tempor. Fugiat laboris enim anim veniam aliquip cillum irure.</div>
			</div>
		</div>
		<div class="col-12 mt-2">
			<p class="subheader"><span>Pills</span></p>
		</div>
		<div class="col-12 mb-3">
			<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-expanded="true">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-expanded="true">Profile</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" id="pills-dropdown1-tab" href="#pills-dropdown1" role="tab" data-toggle="pill" aria-controls="pills-dropdown1">@fat</a>
						<a class="dropdown-item" id="pills-dropdown2-tab" href="#pills-dropdown2" role="tab" data-toggle="pill" aria-controls="pills-dropdown2">@mdo</a>
					</div>
				</li>
			</ul>
			<div class="tab-content" id="pills-tabContent">
				<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">Consequat occaecat ullamco amet non eiusmod nostrud dolore irure incididunt est duis anim sunt officia. Fugiat velit proident aliquip nisi incididunt nostrud exercitation proident est nisi. Irure magna elit commodo anim ex veniam culpa eiusmod id nostrud sit cupidatat in veniam ad. Eiusmod consequat eu adipisicing minim anim aliquip cupidatat culpa excepteur quis. Occaecat sit eu exercitation irure Lorem incididunt nostrud.</div>
				<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">Ad pariatur nostrud pariatur exercitation ipsum ipsum culpa mollit commodo mollit ex. Aute sunt incididunt amet commodo est sint nisi deserunt pariatur do. Aliquip ex eiusmod voluptate exercitation cillum id incididunt elit sunt. Qui minim sit magna Lorem id et dolore velit Lorem amet exercitation duis deserunt. Anim id labore elit adipisicing ut in id occaecat pariatur ut ullamco ea tempor duis.</div>
				<div class="tab-pane fade" id="pills-dropdown1" role="tabpanel" aria-labelledby="pills-dropdown1-tab">Est quis nulla laborum officia ad nisi ex nostrud culpa Lorem excepteur aliquip dolor aliqua irure ex. Nulla ut duis ipsum nisi elit fugiat commodo sunt reprehenderit laborum veniam eu veniam. Eiusmod minim exercitation fugiat irure ex labore incididunt do fugiat commodo aliquip sit id deserunt reprehenderit aliquip nostrud. Amet ex cupidatat excepteur aute veniam incididunt mollit cupidatat esse irure officia elit do ipsum ullamco Lorem. Ullamco ut ad minim do mollit labore ipsum laboris ipsum commodo sunt tempor enim incididunt. Commodo quis sunt dolore aliquip aute tempor irure magna enim minim reprehenderit. Ullamco consectetur culpa veniam sint cillum aliqua incididunt velit ullamco sunt ullamco quis quis commodo voluptate. Mollit nulla nostrud adipisicing aliqua cupidatat aliqua pariatur mollit voluptate voluptate consequat non.</div>
				<div class="tab-pane fade" id="pills-dropdown2" role="tabpanel" aria-labelledby="pills-dropdown2-tab">Tempor anim aliquip qui nisi sit minim ex in cupidatat ipsum adipisicing. Ad non magna anim id ullamco do dolor sit adipisicing nulla exercitation. Qui Lorem eiusmod sint in laboris pariatur est adipisicing non sunt occaecat in mollit culpa sit. Aliquip id duis do do quis mollit ut duis. Non dolor reprehenderit do esse nostrud deserunt non eiusmod minim anim sit voluptate ipsum. Nulla elit aliqua do sunt labore velit anim nisi dolor nostrud consectetur fugiat ex qui velit ex tempor. Do cillum qui anim aliquip id cillum duis ex laboris tempor incididunt sint dolor ullamco tempor. Fugiat laboris enim anim veniam aliquip cillum irure.</div>
			</div>
		</div>
		<div class="col-12 mt-2">
			<p class="subheader"><span>Vertical Pills</span></p>
		</div>
		<div class="col-12 mb-3">
			<div class="row">
				<div class="col-3">
					<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist">
						<a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-expanded="true">Home</a>
						<a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-expanded="true">Profile</a>
						<a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-expanded="true">Messages</a>
						<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-expanded="true">Settings</a>
					</div>
				</div>
				<div class="col-9">
					<div class="tab-content" id="v-pills-tabContent">
					<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">Consequat occaecat ullamco amet non eiusmod nostrud dolore irure incididunt est duis anim sunt officia. Fugiat velit proident aliquip nisi incididunt nostrud exercitation proident est nisi. Irure magna elit commodo anim ex veniam culpa eiusmod id nostrud sit cupidatat in veniam ad. Eiusmod consequat eu adipisicing minim anim aliquip cupidatat culpa excepteur quis. Occaecat sit eu exercitation irure Lorem incididunt nostrud.</div>
					<div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">Ad pariatur nostrud pariatur exercitation ipsum ipsum culpa mollit commodo mollit ex. Aute sunt incididunt amet commodo est sint nisi deserunt pariatur do. Aliquip ex eiusmod voluptate exercitation cillum id incididunt elit sunt. Qui minim sit magna Lorem id et dolore velit Lorem amet exercitation duis deserunt. Anim id labore elit adipisicing ut in id occaecat pariatur ut ullamco ea tempor duis.</div>
					<div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">Est quis nulla laborum officia ad nisi ex nostrud culpa Lorem excepteur aliquip dolor aliqua irure ex. Nulla ut duis ipsum nisi elit fugiat commodo sunt reprehenderit laborum veniam eu veniam. Eiusmod minim exercitation fugiat irure ex labore incididunt do fugiat commodo aliquip sit id deserunt reprehenderit aliquip nostrud. Amet ex cupidatat excepteur aute veniam incididunt mollit cupidatat esse irure officia elit do ipsum ullamco Lorem. Ullamco ut ad minim do mollit labore ipsum laboris ipsum commodo sunt tempor enim incididunt. Commodo quis sunt dolore aliquip aute tempor irure magna enim minim reprehenderit. Ullamco consectetur culpa veniam sint cillum aliqua incididunt velit ullamco sunt ullamco quis quis commodo voluptate. Mollit nulla nostrud adipisicing aliqua cupidatat aliqua pariatur mollit voluptate voluptate consequat non.</div>
					<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">Tempor anim aliquip qui nisi sit minim ex in cupidatat ipsum adipisicing. Ad non magna anim id ullamco do dolor sit adipisicing nulla exercitation. Qui Lorem eiusmod sint in laboris pariatur est adipisicing non sunt occaecat in mollit culpa sit. Aliquip id duis do do quis mollit ut duis. Non dolor reprehenderit do esse nostrud deserunt non eiusmod minim anim sit voluptate ipsum. Nulla elit aliqua do sunt labore velit anim nisi dolor nostrud consectetur fugiat ex qui velit ex tempor. Do cillum qui anim aliquip id cillum duis ex laboris tempor incididunt sint dolor ullamco tempor. Fugiat laboris enim anim veniam aliquip cillum irure.</div>
				</div>
				</div>
			</div>
		</div>
	</div>

</div>

<i class="fa fa-battery-three-quarters" aria-hidden="true"></i>

<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			$('[data-toggle="popover"]').popover({trigger: 'focus'});
		});
	})(jQuery);
</script>