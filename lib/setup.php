<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage')
  ]);

  register_nav_menus([
    'primary_navigation' => __('Secondary Navigation', 'sage')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));

  // Remove the generator tag
  remove_action('wp_head', 'wp_generator');

  // Remove emoji script
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );

  // Remove rest_output_link_wp_head, wp_oembed_add_discovery_links
  // and rest_output_link_header tags
  remove_action( 'wp_head', 'rest_output_link_wp_head' );
  remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
  remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );

  // Remove Weblog Client Link
  remove_action ('wp_head', 'rsd_link');

  // Remove Windows Live Writer Manifest Link
  remove_action( 'wp_head', 'wlwmanifest_link');

  // Remove WordPress Page/Post Shortlinks
  remove_action( 'wp_head', 'wp_shortlink_wp_head');

  // Remove Admin bar css
  remove_action('wp_head', '_admin_bar_bump_cb');

  // Remove auto <p> tag
  remove_filter( 'the_content', 'wpautop' );

  // Oculta el menu ACF (Descomentar cuando el sitio se lance a producción)
  // add_filter('acf/settings/show_admin', '__return_false');
  // Oculta las actualizaciones de ACF */
  add_filter( 'acf/settings/show_updates', '__return_false' );
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
  if ( strpos( $src, 'ver=' ) )
    $src = remove_query_arg( 'ver', $src );
  return $src;
}
add_filter( 'style_loader_src', __NAMESPACE__ . '\\vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', __NAMESPACE__ . '\\vc_remove_wp_ver_css_js', 9999 );

function remove_recent_comments_style() {
  global $wp_widget_factory;
  remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}
add_action('widgets_init', __NAMESPACE__ . '\\remove_recent_comments_style');

// Change Footer text
function remove_footer_admin () {
    echo 'Powered by <a href="http://oohmd.com" target="_blank" rel="noopener noreferrer">OOH! Marketing</a>';
}
add_filter('admin_footer_text', __NAMESPACE__ . '\\remove_footer_admin');
add_filter('admin_upgrade_text', __NAMESPACE__ . '\\remove_footer_admin');

function hide_plugin_trickspanda() {
  global $wp_list_table;
  $hidearr = array('advanced-custom-fields-pro/acf.php', 'acf-quick-edit-fields/index.php');
  $myplugins = $wp_list_table->items;
  foreach ($myplugins as $key => $val) {
    if (in_array($key,$hidearr)) {
      unset($wp_list_table->items[$key]);
    }
  }
}
add_action('pre_current_active_plugins', __NAMESPACE__ . '\\hide_plugin_trickspanda');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Blog Sidebar', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Footer 1', 'sage'),
    'id'            => 'sidebar-footer1',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Footer 2', 'sage'),
    'id'            => 'sidebar-footer2',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Footer 3', 'sage'),
    'id'            => 'sidebar-footer3',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Footer 4', 'sage'),
    'id'            => 'sidebar-footer4',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    is_page_template('template-sidebar.php'),
    !is_home()
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
  wp_enqueue_style('theme/css', Assets\asset_path('styles/main.css'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_enqueue_script('theme/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

/**
 * Admin assets
 */
function admin_assets() {
  wp_enqueue_style('theme/admin', Assets\asset_path('styles/admin.css'), false, '1.0', 'all');
}
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\\admin_assets' );
