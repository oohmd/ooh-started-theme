<?php
/**
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 */
require_once get_template_directory() . '/lib/tgm/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'ooh_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 */
function ooh_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		array(
			'name'		=> 'ACF Pro',
			'slug'		=> 'advanced-custom-fields-pro',
			'source'	=> 'advanced-custom-fields-pro.zip',
			'required'	=> true,
		),
		array(
			'name'		=> 'ACF Quick Edit',
			'slug'		=> 'acf-quick-edit-fields',
			'source'	=> 'acf-quick-edit-fields.zip',
			'required'	=> true,
		),
		array(
			'name'		=> 'SMTP Mailer',
			'slug'		=> 'smtp-mailer',
		),
		array(
			'name'		=> 'Contact Form 7',
			'slug'		=> 'contact-form-7',
		),
		array(
			'name'		=> 'Smart Slider 3',
			'slug'		=> 'smart-slider-3',
		),
		array(
			'name'		=> 'All in One SEO Pack',
			'slug'		=> 'all-in-one-seo-pack',
		),
		array(
			'name'		=> 'Smush Image Compression and Optimization',
			'slug'		=> 'wp-smushit'
		),

	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 */
	$config = array(
		'id'			=> 'oohtheme',
		'default_path'	=> get_template_directory() . '/lib/tgm/plugins/',
		'menu'			=> 'ooh-install-plugins',
		'parent_slug'	=> 'themes.php',
		'capability'	=> 'edit_theme_options',
		'has_notices'	=> true,
		'dismissable'	=> true,
		'dismiss_msg'	=> '',
		'is_automatic'	=> true,
	);

	tgmpa( $plugins, $config );
}
